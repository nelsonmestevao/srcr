% vim: ft=prolog
%------------------------------------------------------------------------------
% SICStus PROLOG: Declarações iniciais ----------------------------------------
%------------------------------------------------------------------------------

% :- set_prolog_flag( discontiguous_warnings,off ).
% :- set_prolog_flag( single_var_warnings,off    ).
% :- set_prolog_flag( unknown,fail               ).
% :- op( 900,xfy,'::' ).

% Tuples -----------------------------------------------------------------------

fst((X, _), X).
fst((A, _, _), A).
fst((A, _, _, _), A).
fst((A, _, _, _, _, _), A).

snd((_, Y), Y).
snd((_, Y, _), Y).
snd((_, Y, _, _, _, _), Y).

trd((_, _, Z, _, _, _), Z).
trd((_, _, Z), Z).

rth((_, _, _, W, _, _), W).

fth((_, _, _, _, K, _), K).

append_length(L, R) :- length(L, C), R = (L, C).

% Lists -----------------------------------------------------------------------

head([H | _], H).
tail([_ | T], T).

map(_, [], []).
map(Predicate, [X|Xs], [Y|Ys]) :-
   call(Predicate, X, Y),
   map(Predicate, Xs, Ys).

% recebendo uma lista de pares, devolve uma lista só com os primeiros elementos
map_fst([],[]).
map_fst([(X,_)|T], R) :- map_fst(T, L), R = [X|L].

map_snd([],[]).
map_snd([(_,Y)|T], R) :- map_fst(T, L), R = [Y|L].

% Geometry --------------------------------------------------------------------

% distancia euclidiana
distance((X, Y), (K, W), Distance) :-
   DX is exp((X - K), 2), DY is exp((Y - W), 2),
   Distance is sqrt(DX + DY).
