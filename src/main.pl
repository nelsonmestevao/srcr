% vim: ft=prolog
%------------------------------------------------------------------------------
% SICStus PROLOG: Declarações iniciais ----------------------------------------
%------------------------------------------------------------------------------

% :- set_prolog_flag( discontiguous_warnings,off ).
% :- set_prolog_flag( single_var_warnings,off    ).
% :- set_prolog_flag( unknown,fail               ).
% :- op( 900,xfy,'::' ).

%------------------------------------------------------------------------------
% Knowladge Base --------------------------------------------------------------
%------------------------------------------------------------------------------

:- include('arestas.pl').
:- include('pontos.pl').
:- include('paragens.pl').

%------------------------------------------------------------------------------
% Modules ---------------------------------------------------------------------
%------------------------------------------------------------------------------

% :- use_module(library(lists)).
:- include('utils.pl').

% Geral -----------------------------------------------------------------------

adjacente(Ponto_A, Ponto_B, grafo(_, Arestas)) :- member(aresta(Ponto_A, Ponto_B), Arestas).
adjacente(Ponto_A, Ponto_B, grafo(_, Arestas)) :- member(aresta(Ponto_B, Ponto_A), Arestas).

adjacentes(Ponto, Grafo, Lista_Adjacencia) :-
    findall(X, adjacente(X, Ponto, Grafo), Lista_Adjacencia).

%//TODO: verificar numero de campos
obter_id((ID, _, _, _, _, _), ID).

% Utilitários Auxiliares -----------------------------------------------------

segundo_da_cabeca([], 0).
segundo_da_cabeca(L, R) :- head(L, P), snd(P, R).

%% de uma lista de pares, obtem só os pares em que o segunda parte do par é a maior
lista_maiores([],[]).
lista_maiores([(X, Y) | T], R) :- lista_maiores(T, M), segundo_da_cabeca(M, P),
    Y > P, R = [(X, Y)].
lista_maiores([(X, Y) | T], R) :- lista_maiores(T, M), segundo_da_cabeca(M, P),
    Y == P, append(M, [(X, Y)], R).
lista_maiores([(_, Y) | T], R) :- lista_maiores(T, M), segundo_da_cabeca(M, P),
    Y < P, R = M.

% de uma lista de pares, obtem só os pares em que o segunda parte do par é a menor
lista_menores([],[]).
lista_menores([X],[X]).
lista_menores([(_, Y) | T], R) :- lista_menores(T, M), segundo_da_cabeca(M, P),
    Y > P, R = M.
lista_menores([(X, Y) | T], R) :- lista_menores(T, M), segundo_da_cabeca(M, P),
    Y == P, append(M, [(X, Y)], R).
lista_menores([(X, Y) | T], R) :- lista_menores(T, M), segundo_da_cabeca(M, P),
    Y < P, R = [(X, Y)].

% remove arestas com pontos contidos na lista ToRemove
rm_edges([], _, []).
rm_edges([aresta(X, _) | Edges], ToRemove, Result) :- member(X, ToRemove), !, rm_edges(Edges, ToRemove, Result).
rm_edges([aresta(_, Y) | Edges], ToRemove, Result) :- member(Y, ToRemove), !, rm_edges(Edges, ToRemove, Result).
rm_edges([aresta(X, Y) | Edges], ToRemove, [aresta(X, Y) | Rest]) :- rm_edges(Edges, ToRemove, Rest).

% recebe uma lista com tuplos e remove os que tem um campo (Criteria) na lista de remoção (ToRemove)
filter([], _, _, []).
filter([Point | Points], Criteria, ToRemove, Result) :-
    call(Criteria, Point, Y), member(Y, ToRemove), !,
    filter(Points, Criteria, ToRemove, Result).
filter([Point | Points], Criteria, ToRemove, [ID | Rest]) :-
    filter(Points, Criteria, ToRemove, Rest), obter_id(Point, ID).

% recebe uma lista com tuplos e remove os que não tem um campo (Criteria) na lista de seleção (ToSelect)
select([], _, _, []).
select([Point | Points], Criteria, ToSelect, [ID | Rest]) :-
    call(Criteria, Point, Y), member(Y, ToSelect), !,
    select(Points, Criteria, ToSelect, Rest), obter_id(Point, ID).
select([_ | Points], Criteria, ToSelect, Result) :-
    select(Points, Criteria, ToSelect, Result).

% -----------------------------------------------------------------------------
% 1. Calcular um trajeto entre dois pontos;

caminho(Grafo, Ponto_X, Ponto_Y, Caminho) :- caminho_aux(Grafo, Ponto_X, [Ponto_Y], Caminho).

caminho_aux(_,A,[A|P1],[A|P1]).
caminho_aux(G,A,[Y|P1],P) :-
    adjacente(X,Y,G), \+ memberchk(X,[Y|P1]), caminho_aux(G,A,[X,Y|P1],P).

caminhos(Grafo, Origem, Destino, Caminhos) :- adjacentes(Origem, Grafo, Adjacentes),
    caminhos_aux(Grafo, Origem, Destino, Adjacentes, Caminhos).

caminhos_aux(_, _, _, [], []).
caminhos_aux(Grafo, Origem, Destino, [Adjacente | Adjacentes], Caminhos) :-
    caminho(Grafo, Adjacente, Destino, Caminho),
    caminhos_aux(Grafo, Origem, Destino, Adjacentes, Restantes_Caminhos),
    append([Origem], Caminho, Caminho_Completo),
    append([Caminho_Completo], Restantes_Caminhos, Caminhos).

% -----------------------------------------------------------------------------
% 2. Selecionar apenas algumas das operadoras de transporte para um determinado percurso;

caminho_select_operadoras(grafo(Pontos, Arestas), Origem, Destino, Operadoras, Caminho) :-
    filter(Pontos, fth, Operadoras, Pontos_Filtrados), rm_edges(Arestas, Pontos_Filtrados, Arestas_Filtradas),
    caminho(grafo(Pontos, Arestas_Filtradas), Origem, Destino, Caminho).

% -----------------------------------------------------------------------------
% 3. Excluir um ou mais operadores de transporte para o percurso;

caminho_filter_operadoras(grafo(Pontos, Arestas), Origem, Destino, Operadoras, Caminho) :-
    select(Pontos, fth, Operadoras, Pontos_Filtrados), rm_edges(Arestas, Pontos_Filtrados, Arestas_Filtradas),
    caminho(grafo(Pontos, Arestas_Filtradas), Origem, Destino, Caminho).

% -----------------------------------------------------------------------------
% 4. Identificar quais as paragens com o maior número de carreiras num determinado percurso.

lista_paragens_com_max_nr_carreiras(Grafo, Ponto_A, Ponto_B, L) :-
    caminho(Grafo, Ponto_A, Ponto_B, Path),
    obtem_comprimentos_listas_carreiras(Grafo, Path, R1),
    lista_maiores(R1, R2), map_fst(R2, L).

obtem_comprimentos_listas_carreiras(_, [], []).
obtem_comprimentos_listas_carreiras(G, [H | T], L) :-
    comprimento_lista_carreiras(G, H, Comprimento),
    obtem_comprimentos_listas_carreiras(G, T, B),
    append([Comprimento], B, L).

comprimento_lista_carreiras(Grafo, Ponto, R) :-
    devolve_lista_carreiras(Grafo, Ponto, (_, R1)), length(R1, Comprimento),
    R = (Ponto, Comprimento).

%//TODO: verificar numero de campos
devolve_lista_carreiras(grafo([(X, _, _, _, _, Y) | T], Arestas), Ponto, R) :-
    Ponto == X, R = (Ponto, Y);
    devolve_lista_carreiras(grafo(T, Arestas), Ponto, R).

% -----------------------------------------------------------------------------
% 5. Escolher o menor percurso (usando critério menor número de paragens);

caminho_menos_paragens(Grafo, Origem, Destino, Caminho) :-
    caminhos(Grafo, Origem, Destino, Caminhos), map(append_length, Caminhos, Resultado),
    lista_menores(Resultado, [(Caminho, _) | _]);
        caminho(Grafo, Origem, Destino, Caminho).

% -----------------------------------------------------------------------------
% 6. Escolher o percurso mais rápido (usando critério da distância);

caminho_mais_rapido(Grafo, Origem, Destino, Caminho) :-
    caminhos(Grafo, Origem, Destino, Caminhos), append_distance(Grafo, Caminhos, Resultado),
    lista_menores(Resultado, [(Path, _) | _]), map(obter_id, [Path], Caminho);
    caminho(Grafo, Origem, Destino, Caminho).

append_distance(Grafo, Caminhos, Resultado) :-
    map(append_coordenadas(Grafo), Caminhos, Lista), map(append_distance_aux, Lista, Resultado).

append_distance_aux(Caminho, Resultado) :-
    distancia_caminho(Caminho, Distancia), Resultado = (Caminho, Distancia).

append_coordenadas(Grafo, Pontos, Resultado) :-
    map(append_coordenadas_aux(Grafo), Pontos, Resultado).

append_coordenadas_aux(Grafo, Ponto, Resultado) :-
    devolve_coordenadas(Grafo, Ponto, Coordenadas),
    Resultado = (Ponto, Coordenadas).

devolve_coordenadas([],_,(_,_)).
devolve_coordenadas(grafo([(A, B) | T], _), X, Coordenadas) :-
     X == A, Coordenadas = B;
     devolve_coordenadas(grafo(T, _), X, Coordenadas).

%//TODO: ?verificar numero de campos?
distancia_caminho([], 0).
distancia_caminho([_], 0).
distancia_caminho([(_, Y, _, _, _, _), (K, W, U, S, Q, V) | Pontos], Distancia) :-
    distance(Y, W, R1),
    distancia_caminho([(K, W, U, S, Q, V) | Pontos], R2), Distancia is R1+R2.

% -----------------------------------------------------------------------------
% 7. Escolher o percurso que passe apenas por abrigos com publicidade;

caminho_com_publicidade(grafo(Pontos, Arestas), Origem, Destino, Caminho) :-
    filter(Pontos, rth, ['Yes'], Pontos_Filtrados), rm_edges(Arestas, Pontos_Filtrados, Arestas_Filtradas),
    caminho(grafo(Pontos, Arestas_Filtradas), Origem, Destino, Caminho).

% -----------------------------------------------------------------------------
% 8. Escolher o percurso que passe apenas por paragens abrigadas;

caminho_abrigado(grafo(Pontos, Arestas), Origem, Destino, Caminho) :-
    filter(Pontos, trd, ['Fechado dos Lados', 'Aberto dos Lados'], Pontos_Filtrados), rm_edges(Arestas, Pontos_Filtrados, Arestas_Filtradas),
    caminho(grafo(Pontos, Arestas_Filtradas), Origem, Destino, Caminho).

% -----------------------------------------------------------------------------
% 9. Escolher um ou mais pontos intermédios por onde o percurso deverá passar.

caminho_passando_em_pontos(Grafo, Ponto_Inicial, [], Ponto_Final, Caminho) :-
    caminho(Grafo, Ponto_Inicial, Ponto_Final, Caminho).
caminho_passando_em_pontos(Grafo, Ponto_Inicial, [Ponto | Pontos], Ponto_Final, Caminho) :-
    caminho(Grafo, Ponto_Inicial, Ponto, Percurso_1),
    caminho_passando_em_pontos(Grafo, Ponto, Pontos, Ponto_Final, Percurso_Restante),
    tail(Percurso_Restante, Cauda), append(Percurso_1, Cauda, Caminho).
