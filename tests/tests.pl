% vim: ft=prolog
%------------------------------------------------------------------------------
% SICStus PROLOG: Declarações iniciais ----------------------------------------
%------------------------------------------------------------------------------

% :- set_prolog_flag( discontiguous_warnings,off ).
% :- set_prolog_flag( single_var_warnings,off    ).
% :- set_prolog_flag( unknown,fail               ).
% :- op( 900,xfy,'::' ).

%------------------------------------------------------------------------------
% Modules ---------------------------------------------------------------------
%------------------------------------------------------------------------------

:- include('../src/main.pl').

%------------------------------------------------------------------------------
% Testes Ponto 1

teste_existe_caminho(Grafo, P1, P2, Lista_Esperada) :- write('Teste existe caminho:'),
    caminho(Grafo, P1, P2, Resultado),
    check(Lista_Esperada, Resultado), success(yes); success(no).

teste_nao_existe_caminho(Grafo, P1, P2) :- write('Teste nao existe caminho:'),
    caminho(Grafo, P1, P2, _),
    success(no); success(yes).

%------------------------------------------------------------------------------
% Testes Ponto 2

teste_caminho_com_certas_operadoras(Grafo, Ponto_1, Ponto_2, Operadoras, Lista_Esperada) :-
    write('Teste caminho usando apenas operadoras escolhidas:'),
    caminho_select_operadoras(Grafo, Ponto_1, Ponto_2, Operadoras, Resultado),
    check(Lista_Esperada, Resultado), success(yes); success(no).

%------------------------------------------------------------------------------
% Testes Ponto 3

teste_caminho_sem_certas_operadoras(Grafo, Ponto_1, Ponto_2, Operadoras, Lista_Esperada) :-
    write('Teste caminho excluindo operadoras:'),
    caminho_filter_operadoras(Grafo, Ponto_1, Ponto_2, Operadoras, Resultado),
    check(Lista_Esperada, Resultado), success(yes); success(no).

%------------------------------------------------------------------------------
% Testes Ponto 4

teste_lista_paragens_max_carreiras(Grafo, Ponto_1, Ponto_2, Lista_Esperada) :-
    write('Teste lista carreiras com maior nr de paragens:'),
    lista_paragens_com_max_nr_carreiras(Grafo, Ponto_1, Ponto_2, Resultado),
    check(Lista_Esperada, Resultado), success(yes); success(no).

%------------------------------------------------------------------------------
% Testes Ponto 5

teste_caminho_menos_paragens(Grafo, Ponto_1, Ponto_2, Lista_Esperada) :-
    write('Teste caminho com menos paragens:'),
    caminho_menos_paragens(Grafo, Ponto_1, Ponto_2, Resultado),
    check(Lista_Esperada, Resultado), success(yes); success(no).

%------------------------------------------------------------------------------
% Testes Ponto 6

teste_caminho_mais_rapido(Grafo, Ponto_1, Ponto_2, Lista_Esperada) :-
    write('Teste caminho mais rapido (menor distancia):'),
    caminho_mais_rapido(Grafo, Ponto_1, Ponto_2, Resultado),
    check(Lista_Esperada, Resultado), success(yes); success(no).

%------------------------------------------------------------------------------
% Testes Ponto 7
teste_caminho_com_publicidade(Grafo, Ponto_1, Ponto_2, Lista_Esperada) :-
    write('Teste caminho so com publicidade:'),
    caminho_com_publicidade(Grafo, Ponto_1, Ponto_2, Resultado),
    check(Lista_Esperada, Resultado), success(yes); success(no).

%------------------------------------------------------------------------------
% Testes Ponto 8
teste_caminho_abrigado(Grafo, Ponto_1, Ponto_2, Lista_Esperada) :-
    write('Teste caminho abrigado:'),
    caminho_abrigado(Grafo, Ponto_1, Ponto_2, Resultado),
    check(Lista_Esperada, Resultado), success(yes); success(no).

%------------------------------------------------------------------------------
% Testes Ponto 9

teste_existe_caminho_passando_em_pontos_intermedios(Grafo, Ponto_I, Pontos, Ponto_F, Lista_Esperada) :-
    write('Teste caminho passando em pontos intermedios:'),
    caminho_passando_em_pontos(Grafo, Ponto_I, Pontos, Ponto_F, Resultado),
    check(Lista_Esperada, Resultado), success(yes); success(no).

%------------------------------------------------------------------------------
% Utils

dbg(Value) :- write(Value), write('\n').

check(Expected, Result) :- Expected == Result; verbose(on, Expected, Result), fail.

success(yes) :- format('~t~w ~72|~n', 'Success').
success(no) :-  format('~t~w ~72|~n', 'Insuccess'), abort.

verbose(off, _, _).
verbose(on, Expected, Result) :- format('~nEsperado:  ~w~nResultado: ~w~n', [Expected, Result]).

%------------------------------------------------------------------------------
% Test Cases

obter_grafo_simples(grafo(
    [(a, (0,0), 'Fechado dos Lados', 'Yes', 'tub', [1,2,5]), (b, (1,1), 'Fechado dos Lados','Yes', 'tub', [1,2,1]),
     (c, (2,2), 'Sem Abrigo', 'No', 'tub', [1,2]), (d, (3,3), 'Fechado dos Lados','Yes', 'tug', [1,2]),
     (e, (4,4), 'Fechado dos Lados', 'Yes', 'tub', [1,2]), (f, (5,5), 'Fechado dos Lados','Yes', 'tub', [1,2,8]),
     (g, (6,6), 'Fechado dos Lados','Yes', 'tub', [1,2])],
    [aresta(a,b), aresta(c,d), aresta(c,f), aresta(d,f), aresta(f,g)]
)).

obter_grafo_geral(grafo(Pontos, Arestas)) :- pontos(X), arestas(Y), Pontos = X, Arestas = Y.

%------------------------------------------------------------------------------
% Display

separator(=) :- format('~`=t~72|~n', []).
separator(-) :- format('~`-t~72|~n', []).

title(I) :- write('PONTO '), write(I), write('\n\n').

%------------------------------------------------------------------------------
% TESTS

run_tests(1) :-
    separator(=),
    write('                              TESTES                                    \n'),
    separator(=),

    obter_grafo_simples(G_SIMPLES),
    obter_grafo_geral(G_GERAL),

    title(1),
    teste_nao_existe_caminho(G_SIMPLES, a, f),
    teste_existe_caminho(G_SIMPLES, c, g, [c, f, g]),
    write('\n'),
    % teste_nao_existe_caminho(G_GERAL, '507', '147'), esta muito lento
    teste_existe_caminho(G_GERAL, '183', '593', ['183', '791', '595', '182', '499', '593']),
    separator(-),

    title(2),
    %caminho_select_operadoras(G_SIMPLES, c, g, ['tub'], Caminho), dbg(Caminho),
    teste_caminho_com_certas_operadoras(G_SIMPLES, c, g, [tub], [c,f,g]),
    teste_caminho_com_certas_operadoras(G_SIMPLES, f, c, [tub], [f,c]),
    write('\n'),
    teste_caminho_com_certas_operadoras(G_GERAL, '183', '595', ['Vimeca'], ['183', '791', '595']),
    separator(-),

    title(3),
    teste_caminho_sem_certas_operadoras(G_SIMPLES, c, g, ['tug'], [c,f,g]),
    write('\n'),
    teste_caminho_sem_certas_operadoras(G_GERAL, '183', '791', ['LT'], ['183', '791']),
    separator(-),

    title(4),
    teste_lista_paragens_max_carreiras(G_SIMPLES, d, g, [f]),
    write('\n'),
    teste_lista_paragens_max_carreiras(G_GERAL, '183', '791', ['791', '183']),
    teste_lista_paragens_max_carreiras(G_GERAL, '183', '595', ['595']),
    separator(-),

    title(5),
    %caminho_menos_paragens(G_SIMPLES, d, g, Caminho), write(Caminho), write('\n'),
    teste_caminho_menos_paragens(G_SIMPLES, d, g, [d, f, g]),
    separator(-),

    title(6),
    % caminhos(G_SIMPLES, d, g, Caminhos), write(Caminhos), write('\n'),
    % append_distance(G_SIMPLES, Caminhos, Caminhos_mais_distancia), write(Caminhos_mais_distancia), write('\n'),
    %lista_menores(Caminhos_mais_distancia, Resultado), write(Resultado), write('\n'),
    teste_caminho_mais_rapido(G_SIMPLES, d, g, [d, f, g]),
    %write('\n'),
    %adjacentes(G_GERAL, '5', Adjacentes), write(Adjacentes), write('\n'),
    %teste_caminho_mais_rapido(G_GERAL, '185', '107', Resultado), write(Resultado), write('\n'),
    separator(-),

    title(7),
    teste_caminho_com_publicidade(G_SIMPLES, d, g, [d, f, g]),
    separator(-),

    title(8),
    teste_caminho_abrigado(G_SIMPLES, d, g, [d, f, g]),
    separator(-),

    title(9),
    teste_existe_caminho_passando_em_pontos_intermedios(G_SIMPLES, c, [d, f], g, [c, d, f, g]),
    %caminho_passando_em_pontos(G_SIMPLES, c, [d,f], g, C), write(C), write('\n'),
    write('\n'),
    teste_existe_caminho_passando_em_pontos_intermedios(G_GERAL, '185', ['107'], '90', ['185', '89', '107', '250', '261', '597', '953', '609', '799', '171', '172', '162', '161', '156', '734', '159', '155', '741', '742', '686', '687', '87', '154', '709', '1014', '68', '788', '170', '183', '791', '595', '182', '499', '593', '181', '180', '594', '185', '89', '90']),
    separator(-),

    halt.
