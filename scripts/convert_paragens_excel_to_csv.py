#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""Prolog.

Usage:
  converter [--excel <file>]
  converter [--output <file>]
  converter (-h | --help)
  converter --version

Options:
  -h --help        Show this screen.
  --excel <file>   File to import [default: data/paragens.xlsx]
  --output <file>  File where to export [default: data/paragens.csv]
"""


from docopt import docopt
import pandas as pd
import os

if __name__ == "__main__":
    arguments = docopt(__doc__, version="Converter Script 0.1.0")
    output_file = arguments["--output"]
    excel = pd.read_excel(arguments["--excel"])

    excel.to_csv(output_file, encoding='utf-8', sep=';', header=True, mode='w', index=False, quotechar='"', quoting=1)
