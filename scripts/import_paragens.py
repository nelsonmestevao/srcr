#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""Prolog.

Usage:
  import [--file <data>]
  import (-h | --help)
  import --version

Options:
  -h --help      Show this screen.
  --file <data>  Data file to import [default: data/paragens.csv]
"""
from docopt import docopt
import pandas as pd

if __name__ == "__main__":
    arguments = docopt(__doc__, version="Import Script 0.1.0")
    paragens = pd.read_csv(arguments["--file"], sep=";").dropna()

    print('% vim: ft=prolog')
    print('\n% "gid","latitude","longitude","Estado de Conservacao","Tipo de Abrigo","Abrigo com Publicidade?","Operadora","[Carreira]","Codigo de Rua","Nome da Rua","Freguesia"')

    for index, row in paragens.iterrows():
        print(
            "paragem("
            + "'"
            + str(row["gid"])
            + "'"
            + ","
            + str(row["latitude"])
            + ","
            + str(row["longitude"])
            + ","
            + "'"
            + row["Estado de Conservacao"]
            + "'"
            + ","
            + "'"
            + row["Tipo de Abrigo"]
            + "'"
            + ","
            + "'"
            + row["Abrigo com Publicidade?"]
            + "'"
            + ","
            + "'"
            + row["Operadora"]
            + "'"
            + ","
            + "["
            + str(row["Carreira"])
            + "]"
            + ","
            + "'"
            + str(row["Codigo de Rua"])
            + "'"
            + ","
            + "'"
            + row["Nome da Rua"]
            + "'"
            + ","
            + "'"
            + row["Freguesia"]
            + "'"
            + ")."
        )
