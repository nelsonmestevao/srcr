#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""Prolog.

Usage:
  converter [--excel <file>]
  converter [--output <file>]
  converter (-h | --help)
  converter --version

Options:
  -h --help        Show this screen.
  --excel <file>   File to import [default: data/adjacencias.xlsx]
  --output <file>  File where to export [default: data/adjacencias.csv]
"""


from docopt import docopt
import pandas as pd
import os

if __name__ == "__main__":
    arguments = docopt(__doc__, version="Converter Script 0.1.0")
    output_file = arguments["--output"]
    excel = pd.read_excel(arguments["--excel"], sheet_name=None)

    with open(output_file, 'w') as csv:
        csv.write('"gid";"latitude";"longitude";"Estado de Conservacao";"Tipo de Abrigo";"Abrigo com Publicidade?";"Operadora";"Carreira";"Codigo de Rua";"Nome da Rua";"Freguesia"\n')

    for sheet in excel.values():
        sheet.to_csv(output_file, encoding='utf-8', sep=';', header=False, mode='a', index=False, quotechar='"', quoting=1)
