#!/usr/bin/env bash

FILE=${1:-"src/paragens.pl"}
OUT=${2:-"src/pontos.pl"}

echo '% vim: ft=prolog' >"$OUT"
echo '' >>"$OUT"
echo 'pontos([' >>"$OUT"

tail -n +4 "$FILE" |
  cut -d "(" -f2 | cut -d ")" -f1 | cut -d "]" -f 1 | sed 's/$/]/g' |
  sed "s/',/'|/g" | sed -r "s/([0-9]),'/\1|'/g" |
  cut -d"|" -f1,2,4,5,6,7 | sed "s/|/,(/" | sed "s/|/),/" |
  sed "s/|/,/g" | sed "s/^/(/" | sed "s/$/),/" \
  >>"$OUT"

# remover a ultima virgula
sed -i '$ s/.$//' "$OUT"

echo "])." >>"$OUT"
